---
name: Principles
---

Principles act as a reusable standard for teams to measure their work. They replace subjective ideals with a shared understanding of what the result must do for users. Just as guardrails keep you safe and on the road, principles keep teams on the path to achieving their vision. By achieving alignment, principles enable us to scale and build velocity.

There are two kinds of principles:

- The principles that guide the process, which are defined in the [handbook](https://about.gitlab.com/handbook/product/#product-principles).
- The principles that define the output, which are described on this page.

Though we take inspiration from other companies, our principles are defined by looking inward. This perspective ensures they are actionable and effective. Just like the rest of our work, we continually adjust our principles and always strive to make them better. Everyone is welcome to suggest improvements by opening an issue and creating a merge request in our [repository](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com)!

## Sophisticated simplicity

GitLab is a product that supports people in their daily work. We respect the importance of their efforts and avoid unnecessary gimmicks. To that end, we work towards [sophisticated simplicity](https://handbook.gitlab.com/handbook/product/ux/product-designer/#aiming-towards-sophisticated-simplicity) in our product: we make thoughtful choices that streamline complex workflows and functionality. These choices help the user stay focused on what matters most.

There are three principles that help support us in this work.

### Prioritize the product

Though the product is made of many tools, everything should function seamlessly together. Design with this mindset to ensure that you are creating a more connected and coherent experience across the product.

<grid>
  <do>

  Optimize connections between capabilities.

  </do>
  <dont>
  
  Optimize for an isolated use of tools.
  
  </dont>
  <do>

  Use patterns consistently across the product.

  </do>
  <dont>
  
  Use different patterns based on product area.
  
  </dont>
  <do>

  Optimize the broader user journey, even when it crosses product groups, or when you're focused on a particular step of that journey.

  </do>
  <dont>
  
  Optimize for individual interactions or features.
  
  </dont>
</grid>

### Support learning

Design to promote the user's learning and proficiency as they interact and explore. Seek to help them minimize mistakes.

<grid>
  <do>

  Clearly explain how recommendations are generated, especially with [AI features](/usability/ai-human-interaction#be-transparent).

  </do>
  <dont>
  
  Omit explanations for recommendations to simplify the experience.
  
  </dont>
  <do>

  Communicate the status of processes that happen in the background.

  </do>
  <dont>
  
  Only communicate about processes that happen in the foreground, as a direct result of the user's actions
  
  </dont>
  <do>

  Ensure the user has all the information needed to proceed with confidence.

  </do>
  <dont>
  
  Delegate key information to the documentation to minimize the UI.
  
  </dont>
</grid>

### Focus on outcomes

Empathy for the user starts with a deep understanding of their needs and goals. Design for the broader outcomes that the user seeks to achieve, instead of the isolated actions or discrete features used to complete specific tasks.

<grid>
  <do>

  Let the user's needs and intended outcomes drive the change.

  </do>
  <dont>
  
  Let the popularity or impressiveness of a feature or technology drive the change.
  
  </dont>
  <do>

  Prioritize user value.

  </do>
  <dont>
  
  Prioritize technical feasibility or development speed.
  
  </dont>
  <do>

  Design for current essential needs.
  
  </do>
  <dont>
  
  Design to accommodate future, potential needs.
  
  </dont>
</grid>

## References

- [Design systems handbook - Design better](https://www.designbetter.co/design-systems-handbook/expanding-design-system)
- [From purpose to patterns - Alla Kholmatova](https://speakerdeck.com/craftui/from-purpose-to-patterns)
- [What are design principles - principles.design](https://principles.design/#what-are-design-principles)
